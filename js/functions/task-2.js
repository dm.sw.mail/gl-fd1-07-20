// Task 2. FDE
// RU: Создайте функцию comp, которая должна сравнивать значения
//     двух параметров a и b и возвращать 1, если они равны и -1, если они не равны.
//     Используйте Function Definition Expression (FDE).
//     Вызовите функцию до ее объявления.
//     Тестовые данные:
//     a = 'abc', b = 'abc', result = 1
//     a = 'abC', b = 'abc', result = -1
// EN: Create a function comp, which should compare the values
//     of two parameters a and b, and return 1, when they equal and return -1,
//     when they are not equal.
//     UseFunction Definition Expression (FDE).
//     Call a function before it declaration.
//     Test data:
//     a = 'abc', b = 'abc', result = 1
//     a = 'abC', b = 'abc', result = -1

(function() {  

    console.info('Task-2');

    let comp = function(a, b) {
        if (a === b) {
            return 'result = 1';
        } else {
            return 'result = -1';
        }
    };

    let a = 'abc';
    let b = 'abc';

    console.log( comp(a, b) );

    a = 'abc';
    b = 'abC';

    console.log( comp(a, b) );

    console.info(' ');

})();