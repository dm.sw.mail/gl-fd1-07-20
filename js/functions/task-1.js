// Task 1. FDS
// RU: Создайте функцию conc, которая должна конкатенировать значения
//     двух параметров a и b и возвращать строку.
//     Используйте Function Declaration Statement (FDS).
//     Вызовите функцию до ее объявления.
//     Тестовые данные:
//     a = '1', b = '1', result = '11'
//     a = 1, b = 1, result = '11'
// EN: Create a function conc, which should concatenate the values
//     of two parameters a and b and return a string.
//     Use Function Declaration Statement (FDS).
//     Call a function before it declaration.
//     Test data:
//     a = '1', b = '1', result = '11'
//     a = 1, b = 1, result = '11'
(function() {  

    console.info('Task-1');

    let a = '1';
    let b = '1';

    console.log('result = ' + conc(a, b) );

    a = 1;
    b = 1;

    console.log('result = ' + conc(a, b) );

    function conc(x, y) {
        return '' + x + y;
    }

    console.info(' ');

})();