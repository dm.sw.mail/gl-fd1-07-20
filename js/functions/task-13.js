// Task 13
// RU: Преобразовать массив из предыдущего задания в одномерный.
//     Вывести его в консоль
// EN: Transform an array from the previous task into one-dementional array.
//     Display it in the console.

(function() {  

    console.info('Task-13');

    // Создание массива из предыдущего задания 
    let arr = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],
    ];

    for (i = 0; i < arr.length; i++) {
        console.log( arr[i] );
    }

    console.info(' ');

    // Преобразование массива из предыдущего задания в одномерный

    let columnMatrix = [];

    for (i = 0; i < arr.length; i++) {
        for (j = 0; j < arr.length; j++) {            
            columnMatrix.push(arr[i][j]);            
        }               
    }
    
    console.log( columnMatrix );

    console.info(' ');

})();