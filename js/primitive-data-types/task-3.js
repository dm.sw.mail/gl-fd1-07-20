// Task 03
// Объявите три переменных: a, b, c. Присвойте им следующие значения: 10, 2, 5.
// Объявите переменную result1 и вычислите сумму значений переменных a, b, c.
// Объявите переменную min и вычислите минимальное значение переменных a, b, c.
// Выведите результат в консоль.

(function() {  
    
    console.info('Task-3');

    let a = 10;
    let b = 2;
    let c = 5;
    let result1 = a + b + c;
    let min;

    min = ((a < b) && (a < c)) ? a : ((b < a) && (b < c)) ? b : c;
    
    console.log('summa = ' + result1, '; minimum = ' + min);
    
    console.info(' ');

})();