// Task 07
// Объявите переменную a.
// Если значение переменной равно 0, выведите в консоль "True", иначе "False".
// Проверьте, что будет появляться в консоли для значений 1, 0, -3.

(function() {  

    console.info('Task-7');

    let a = 1;

    a = (a === 0) ? true : false;
    console.log(a);

    a = 0;

    a = (a === 0) ? true : false;
    console.log(a);

    a = -3;
    
    a = (a === 0) ? true : false;
    console.log(a);

    console.info(' ');

})();