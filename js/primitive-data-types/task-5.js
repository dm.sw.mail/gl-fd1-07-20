// Task 05
// Объявите переменную minute и проинициализируйте ее целым числом.
// Вычислите к какой четверти принадлежит это число и выведите в консоль.

(function () {

    console.info('Task-5');

    let minute = 44;

    if (minute >= 0  && minute <= 15) {
        console.log('I четверть');
    } else if (minute > 15 && minute <= 30) {
        console.log('II четверть');
    } else if (minute > 30 && minute <= 45) {
        console.log('III четверть');
    } else if (minute <= 60) {
        console.log('IV четверть');
    };

    console.info(' ');

})();