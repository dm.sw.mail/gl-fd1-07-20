// Task 08
// Объявите две переменных: a, b. Вычислите их сумму и присвойте переменной result.
// Если результат больше 5, выведите его в консоль, иначе умножте его на 10
// и выведите в консоль.
// Данные для тестирования: 2, 5 и 3, 1.

(function() {  

    console.info('Task-8');

    let a = 2;
    let b = 5;
    let result = a + b;

    if (result > 5) {
        console.log('result(2, 5) = ' + result);
    } else {
        result *= 10;
        console.log('result(2, 5) = ' + result);
    };

    a = 3;
    b = 1;
    result = a + b;

    if (result > 5) {
        console.log('result(3, 1) = ' + result);
    } else {
        result *= 10;
        console.log('result(3, 1) = ' + result);
    };

    console.info(' ');

})();