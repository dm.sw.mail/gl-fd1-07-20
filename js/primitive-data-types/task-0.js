(function() { 

    // string

    let friendsTravelPlace = 'Fred and Brent spent a week in Spain';
    
    let funSummerActivity = 'It is fun to camp in a tent';
    
    let walkedInDark = 'I kept bumping into things in the dark.';
    
    let interracialFriendsPlay = 'afroamerican boy and white boy play in ring';
    
    let summerFriendsGames = 'to camp in a tent';
    
    let circleDressColor = 'blue';
    
    let clownAndChimpanzeeGame = 'The clown did tricks with a chimpanzee';
    
    let milkAndChildrensTeeth = 'Milk is good for children`s teeth';
    
    let ruleAboutFlowers = 'I must not tramp on the flowers';
    
    let circleColorWhereClownAndChimpanzeeGame = 'rose';
    
    let girlLikeEat = 'apple and milk';
    
    let badBoyAndFlowers = 'Bad boy tramp on the flowers';
    
    let liveFrog = 'The frog jumps in the pond and swims off';
    
    let crabHome = 'a crack in the rock';
    
    let whatCanIHear = 'I can hear twigs snapping in the wind.';
    
    let frogHome = 'pond';
    
    let crabHomes = 'a crack in the rock';
    
    let treeInAutumn = 'yellow and orange tree';
    
    let tapBreakdown = 'A drip from the tap drops in the sink.';
    
    let sinkColor = 'white';

    // object

    let car = {
        color: 'blue',
        size: 'small',
        isFull: true,    
    };

    let dog = {
        color: 'black',
        name: 'Zhuchka',
        isFun: true,
    };

    let hat = {
        color: 'blue-yellow',
        size: 'small',
        isCold: false,
    };

    let paint = {
        color: 'orange',
        isMuch: true,        
    };

    let pirate = {
        name: 'Jhon',
        hasDog: false,        
    };

    let bucket = {
        size: 'big',
        isGreen: true,        
    };

    let crayons = {
        size: 'big',
        areGreen: false,        
    };

    let pumpkin = {
        size: 'big',
        isRed: false,        
    };

    let rabbit = {
        size: 'small',
        isGreen: false,        
    };

    let banana = {
        size: 'small',
        isGreen: false,        
    };

    let camera = {
        size: 'small',
        isBlack: true,        
    };

    let dinosaur = {
        size: 'small',
        isBrown: true,        
    };

    let elephant = {
        size: 'big',
        isGray: true,        
    };

    let umbrella = {
        size: 'big',
        isOpen: true,        
    };

    let alligator = {
        size: 'big',
        isGreen: true,        
    };

    let helicopter = {
        size: 'big',
        isGreen: false,        
    };

    let television = {
        size: 'big',
        isWork: false,        
    };

    let watermelon = {
        size: 'big',
        isSweet: true,        
    };

    // number

    let iceCreamsCount = 1;

    let shipsCount = 2;

    let underpantsCount = 3;

    let flowersCount = 4;

    let butterflyCount = 5;

    let carsCount = 6;

    let fishCount = 7;

    let dinosaurCount = 8;

    let rabbitCount = 9;

    let appleCount = 10;

    // boolean

    let isComfortFriendGood = true;

    let isApologizeGood = true;

    let isPlayWithFriendsGood = true;

    let isHelpFriendGood = true;

    let isPushFriendGood = false;

    let isScreamAtFriendGood = false;

    let isPlayWithFriendsBad = false;

    let isGiveBallFriendGood = true;

    let isBumpGirlBad = true;

    let isReadWithFriendGood = true;

    let isBumpFriendBad = true;

    let isPlayFootballFriendsGood = true;

    let isScoldGirlfriendOkay = false;
    
    let isOpenDoorFriendBad = false;

    let isBumpFriendGood = false;

    let isDanseFriendGood = true;

    let isBumpFriendsGood = false;

    let isDanseGirlBad = false;

    let isWrongBoyBad = true;

    let isPlayFootballFriendGood = true;

    let isDanseGirlGood = true;
})();