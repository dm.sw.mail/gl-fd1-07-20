// Task 09
// Объявите переменную month и проинициализируйте ее числом от 1 до 12.
// Вычислите время года и выведите его в консоль.

(function() {  

    console.info('Task-9');

    let month = 1;
    let season;

    while (month <= 12) {

        if (month >= 1 && month <= 2) {
            season = 'winter'
        } else if (month >= 3 && month <= 5) {
            season = 'spring'
        } else if (month >= 6 && month <= 8) {
            season = 'summer'
        } else if (month >= 9 && month <= 11) {
            season = 'autumn'
        } else {
            season = 'winter'
        };

        console.log('время года - ' + season);

        ++month;
    };

    console.info(' ');

})();