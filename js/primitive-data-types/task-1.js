// Task 01
// Объявите переменную days и проинициализируйте ее числом от 1 до 10.
// Преобразуйте это число в количество секунд и выведите в консоль.

(function() {  

    console.info('Task-1');

    let days = 10;
    let seconds =  days * 24 *60 * 60;

    console.log(seconds + 'c');

    console.info(' ');

})();