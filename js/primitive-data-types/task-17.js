// Task 17
// Замените for на while без изменения поведения цикла
// for (var i = 0; i < 3; i++) {
//   alert( "номер " + i + "!" );
// }

(function() {  

    console.info('Task-17');

    var i = 0;
    
    while (i < 3) {
        i++;
        alert( "номер " + i + "!" );
    }

    console.info(' ');

})();