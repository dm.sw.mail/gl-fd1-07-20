// Task 25
// 
// Требуется спросить у пользователя:
//     фамилию, имя, отчество РАЗДЕЛЬНО (тремя операторами prompt)
//     возраст в годах (оператором prompt)
//     пол (оператором confirm, например, "ваш пол - мужской?")
// и вывести оператором alert анкету пользователя по примеру:
//     ваше ФИО: Иванов Иван Иванович
//     ваш возраст в годах: 20
//     ваш возраст в днях: 7300
//     через 5 лет вам будет: 25
//     ваш пол: мужской
//     вы на пенсии: нет
// реализовать:
//     с использованием условного оператора (if), без условной операции (?:).
//     с использованием условной операции (?:), без условного оператора (if).
// В обоих реализациях оператор alert должен использоваться ровно один раз.


//     с использованием условного оператора (if), без условной операции (?:).

(function() {  

    console.info('Task-25');

    let firstName = prompt('Ваша фамилия: ', '');
    let middleName = prompt('Ваше имя: ', '');
    let lastName = prompt('Ваше отчество: ', '');
    let age = prompt('Ваш возраст: ', '');
    let sex = confirm('Ваш пол - мужской?');
    let pension;

    if (!firstName) {
        firstName = 'Не заполнено';
    };

    if (!middleName) {
        middleName = 'Не заполнено';
    };

    if (!lastName) {
        lastName = 'Не заполнено';
    };

    if (!age || !(age - 0)) {
        age = 'Не заполнено';
    };

    let days;
    let ageLast;

    if (age - 0) {
        days = age * 365;
        ageLast = +age + 5;
    } else {
        days = 'Не заполнено';
        ageLast = 'Не заполнено';
    };

    if (sex) {
        sex = 'мужской';
    } else {
        sex = 'женский';
    };

    if (age - 0) {
        if (age > 65) {
            pension = 'да';
        } else {
            pension = 'нет';
        };
    } else {
        pension = 'Не заполнено';
    };

    alert(`            ваше ФИО: ${firstName} ${middleName} ${lastName} 
            ваш возраст в годах: ${age}
            ваш возраст в днях: ${days}
            через 5 лет вам будет: ${ageLast}
            ваш пол: ${sex}
            вы на пенсии: ${pension}`);

    console.info(' ');

})();


//     с использованием условной операции (?:), без условного оператора (if).

(function() {  

    console.info('Task-25');

    firstName = prompt('Ваша фамилия: ', '');
    middleName = prompt('Ваше имя: ', '');
    lastName = prompt('Ваше отчество: ', '');
    age = prompt('Ваш возраст: ', '');
    sex = confirm('Ваш пол - мужской?');
    
    firstName = (!firstName) ? 'Не заполнено' : firstName;
    middleName = (!middleName) ? 'Не заполнено' : middleName;
    lastName = (!lastName) ? 'Не заполнено' : lastName;
    
    age = (!age || !(age - 0)) ? 'Не заполнено' : age;    

    days = (age - 0) ? age * 365 : 'Не заполнено';
    ageLast = (age - 0) ? +age + 5 : 'Не заполнено';

    sex = sex ? 'мужской' : 'женский';

    pension = !(age - 0) ? 'Не заполнено': (age > 65) ? 'да' : 'нет';

    alert(`            ваше ФИО: ${firstName} ${middleName} ${lastName} 
            ваш возраст в годах: ${age}
            ваш возраст в днях: ${days}
            через 5 лет вам будет: ${ageLast}
            ваш пол: ${sex}
            вы на пенсии: ${pension}`);

    console.info(' ');

})();